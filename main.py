from pyweb import PYWEB, simple_template, redirect, render_json
from pyweb.view import Controller
from pyweb.session import session

from core.base_view import BaseView, SessionView


class Index(SessionView):
    def get(self, request):
        user = session.get(request, 'user')

        return simple_template("index.html", user=user, message="实验楼，你好")


class Login(BaseView):
    def get(self, request):
        return simple_template("login.html")

    def post(self, request):
        user = request.form['user']
        session.push(request, 'user', user)
        return redirect("/")


class Logout(SessionView):
    def get(self, request):
        session.pop(request, 'user')
        return redirect("/")


class API(BaseView):
    def get(self, request):
        data = {
            'name': 'test_user_001',
            'createdDate': 'someDate',
            'lastLogin': 'someDate'
        }

        return render_json(data)


syl_url_map = [
    {
        'url': '/',
        'view': Index,
        'endpoint': 'index'
    },
    {
        'url': '/login',
        'view': Login,
        'endpoint': 'test'
    },
    {
        'url': '/logout',
        'view': Logout,
        'endpoint': 'logout'
    },
    {
        'url': '/api',
        'view': API,
        'endpoint': 'api'
    }
]

app = PYWEB()

index_controller = Controller('index', syl_url_map)
app.load_controller(index_controller)

app.run()
