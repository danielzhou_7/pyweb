# Pyweb

This is a light-weight web framework based on python.
The framework is designed in a MVC style with support of a simple implementition of session and template engine.
The main purpose of this project is to help myself fully understand the modern WEB server structure by reinventing the wheel.
The project will be continuouly developed to add more functionalities after transferring to public repo from previously private repo.

### Prerequisites

```
pip3 install werkzeug
```

## License
This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details
